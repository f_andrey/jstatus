RUN
---------------------------------------------
add line 
```
, startupHook   = spawn "~/.xmonad/jstatus | dzen2 -x 950 -y 800 -ta r -fn '-*-terminus-*-*-*-*-12-*-*-*-*-*-*' -fg grey70 -bg '#2c2c32' &"
```
in the section defaultConfig .xmonad/xmonad.hs (run second dzen2)

piped brunch
--------------------------------------------
removal hardcoded

Hardcoded
-------------------------------------------

Configuration hardcoded:

```c
#define XBM_PATH "/usr/home/andrey/.dzen2/" (path xbm images)
#define ICON_POWER_AC XBM_PATH "ac.xbm"
#define ICON_POWER_BAT XBM_PATH "bat_full_01.xbm"
#define ICON_VOLUME_HIGH XBM_PATH "spkr_03.xbm"
#define ICON_VOLUME_MUTE XBM_PATH "pkr_02xbm"
#define ICON_LOAD XBM_PATH "cpu.xbm"
```

Depends and info
-------------------------------------------

For compile need install misc/xosd (without xft, default),  x11/dzen2

dir dzen2/ contains examples xbm image, full pack source http://awesome.naquadah.org/wiki/Nice_Icons, Icons made by sm4tik http://sysphere.org/~anrxc/local/images/sm4tik-icon-pack.tar.bz2 
